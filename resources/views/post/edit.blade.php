@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0 pl-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('post.destroyImage', $post) }}" id="post-delete-image" method="post">
                        @csrf
                        @method('DELETE')
                    </form>

                    <form action="{{ route('post.update', $post) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="mb-3">
                            <label for="title" class="form-label">Titre</label>
                            <input type="text" class="form-control" value="{{ $post->title }}" name="title" id="title">
                        </div>
                        <div class="mb-3">
                            <label for="content" class="form-label">Contenu</label>
                            <textarea class="form-control" name="content" id="content" rows="3">{{ $post->content }}</textarea>
                        </div>

                        <div class="mb-3">
                            <img src="{{ $post->image }}" height="100">
                            <br>

                            <a href="#" onclick="event.preventDefault(); document.querySelector('#post-delete-image').submit()" class="text-danger">Supprimer l'image</a>

                            <hr>

                            <label for="image" class="form-label">Image</label>
                            <input type="file" accept="image/*" class="form-control" name="image" id="image">
                        </div>
                        

                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
