@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0 pl-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="#" method="POST">

                        <div class="mb-3">
                            <label for="title" class="form-label">Titre</label>
                            <input disabled type="text" class="form-control" value="{{ $post->title }}" name="title" id="title">
                        </div>
                        <div class="mb-3">
                            <label for="content" class="form-label">Contenu</label>
                            <textarea disabled class="form-control" name="content" id="content" rows="3">{{ $post->content }}</textarea>
                        </div>

                        <div class="mb-3">
                            <img src="{{ $post->image }}" width="100%">
                        
                            <hr>

                            <label for="image" class="form-label">Image</label>
                            <input disabled type="file" accept="image/*" class="form-control" name="image" id="image">
                        </div>
                        
                    </form>

                    <a href="{{ route('post.edit', $post) }}" class="btn btn-primary">Editer</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">

            <div class="card mb-3">
                <div class="card-body">
                    <h3>Avis</h3>

                    <hr>

                    <form action="{{ route('post.opinion.store', $post) }}" method="post">
                        @csrf

                        <div>
                            <select class="form-control" name="mark" id="mark">
                                <option value="1">1/5</option>
                                <option value="2">2/5</option>
                                <option value="3">3/5</option>
                                <option value="4">4/5</option>
                                <option value="5">5/5</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary w-100 mt-1">Valider mon avis</button>
                    </form>
                    
                    @if ($opinion->exists)
                    <p class="mb-0 mt-3 lead">Mon avis : {{ $opinion->mark }} / 5</p>
                    @endif

                    <p class="mb-0 mt-3 lead">Moyenne d'avis {{ number_format($post->opinions_avg_mark, 2, '.', ' ') }} / 5</p>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h3>Commentaire</h3>
                    <hr>

                    <div class="card mb-2">
                        <div class="card-body small">
                            <span class="small">
                                This is some text within a card body.
                            </span>
                        </div>
                        <div class="card-footer small">
                            <span class="small">By Hicham - 24/12/22</span>
                        </div>
                    </div>

                    <form action="#" method="#">
                        <div class="card">
                            <div class="card-body p-0 small">
                                <textarea class="w-100 h-100 border-0" name="comment" id="comment" cols="30" rows="5"></textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-primary mt-1">Ajouter un commentaire</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
