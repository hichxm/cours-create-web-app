@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul class="mb-0 pl-0">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    
                    <a href="{{ route('post.create') }}" class="btn btn-primary">Créer un article</a>
                    
                    <hr>

                    <form action="{{ route('home') }}" method="get">
                        <div class="row">
                            <div class="col-8">
                                <input type="text" name="search" value="{{ request('search') }}" class="form-control">
                            </div>
                            <div class="col-4">
                                <button type="submit" class="w-100 btn btn-primary mb-3">Rechercher</button>
                            </div>
                        </div>
                    </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Titre</th>
                                <th>Contenu</th>
                                <th>Commentaires</th>
                                <th>Mise à jour</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td>
                                        <img src="{{ $post->image }}" height="50">
                                    </td>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->content }}</td>
                                    <td>0</td>
                                    <td>{{ $post->updated_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ route('post.show', $post) }}" class="btn btn-sm btn-primary">Voir</a>
                                        
                                        <a href="{{ route('post.edit', $post) }}" class="btn btn-sm btn-secondary">Editer</a>
                                    
                                        @if ($post->trashed())
                                        <form action="{{ route('post.restore', $post) }}" method="post">
                                            @csrf

                                            <button type="submit" class="btn btn-sm btn-outline-success">Restaurer</button>
                                        </form>

                                        <form action="{{ route('post.forceDestroy', $post) }}" method="post">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">Supprimer définitif</button>
                                        </form>
                                        @else
                                        <form action="{{ route('post.destroy', $post) }}" method="post">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-sm btn-danger">Supprimer</button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
