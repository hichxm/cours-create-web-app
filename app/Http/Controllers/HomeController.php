<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        /** @var User */
        $user = auth()->user();

        $posts = $user
            ->posts()
            ->getQuery()
            ->when($request->query('search'), function(Builder $builder, $search){
                $builder->search($search);
            })
            ->latest('updated_at')
            ->withTrashed()
            ->get();

        return view('home', [
            'posts' => $posts,
        ]);
    }
}
