<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{

    public function create(Request $request)
    {
        return view('post.create');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'min:5'],
            'content' => ['required', 'min:10'],
        ]);

        $title = $request->get('title');
        $content = $request->get('content');

        $post = new Post();
        $post->user_id = auth()->id();
        $post->title = $title;
        $post->content = $content;
        $post->save();

        return redirect()->route('post.edit', $post);
    }

    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => ['required', 'min:5'],
            'content' => ['required', 'min:10'],
            'image' => ['image', 'max:2048'],
        ]);

        if($request->hasFile('image')) {
            $file = $request->file('image');

            $path = $file->store('/public/posts');

            $post->image = Str::of($path)->replace('public', 'storage')->start('/');
        }

        $title = $request->get('title');
        $content = $request->get('content');

        $post->title = $title;
        $post->content = $content;
        $post->save();

        return redirect()->back();
    }

    public function edit(Request $request, Post $post)
    {
        return view('post.edit', [
            'post' => $post,
        ]);
    }

    public function show(Request $request, Post $post)
    {
        $opinion = $post
            ->opinions()
            ->getQuery()
            ->where('user_id', auth()->id())
            ->firstOrNew();

        $post->loadAvg('opinions', 'mark');

        return view('post.show', [
            'post' => $post,
            'opinion' => $opinion,
        ]);
    }

    public function destroy(Request $request, Post $post)
    {
        $post->delete();

        return redirect()->back();
    }

    public function destroyImage(Request $request, Post $post)
    {
        Storage::delete(storage_path('app' . Str::of($post->image)->replace('storage', 'public')));

        $post->image = null;
        $post->save();

        return redirect()->back();
    }

    public function forceDestroy(Request $request, $postId)
    {
        Post::query()->withTrashed()->findOrFail($postId)->forceDelete();

        return redirect()->back();
    }

    public function restore(Request $request, $postId)
    {
        Post::query()->withTrashed()->findOrFail($postId)->restore();

        return redirect()->back();
    }

}
