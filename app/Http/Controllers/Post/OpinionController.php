<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Opinion;
use App\Models\Post;
use Illuminate\Http\Request;

class OpinionController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $request->validate([
            'mark' => ['required', 'numeric', 'in:1,2,3,4,5'],
        ]);

        $user = $request->user();

        $post
            ->opinions()
            ->getQuery()
            ->updateOrCreate([
                'user_id' => $user->id,
                'post_id' => $post->id,
            ], [
                'mark' => $request->get('mark'),
            ]);

        return redirect()->back();
    }

}
