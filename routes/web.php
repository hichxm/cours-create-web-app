<?php

use App\Http\Controllers\Post\OpinionController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'verify' => true,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');

Route::middleware('auth')->group(function(){
    
    Route::resource('post', PostController::class);
    Route::resource('post.opinion', OpinionController::class)->only('store');
    Route::post('/post/{post}/restore', [PostController::class, 'restore'])->name('post.restore');
    Route::delete('/post/{post}/force', [PostController::class, 'forceDestroy'])->name('post.forceDestroy');
    Route::delete('/post/{post}/destroyImage', [PostController::class, 'destroyImage'])->name('post.destroyImage');

});